package cucumber.demo;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Eleve {
	private String nom;
	private double noteMoyenne;
	private Map<String, Integer> noteModules= new HashMap<String, Integer>();
	
	public String getNom() {return nom;}
	public void setNom(String nom) {this.nom = nom;}
	public double getNoteMoyenne() {return noteMoyenne;}
	public void setNoteMoyenne(double noteMoyenne) {this.noteMoyenne = noteMoyenne;}
	public Map<String, Integer> getNoteModules() {return noteModules;}
	public void setNoteModules(Map<String, Integer> noteModules) {this.noteModules = noteModules;}
}
