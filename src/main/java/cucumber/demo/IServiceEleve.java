package cucumber.demo;

public interface IServiceEleve {
	public boolean getResultatExamen(Eleve eleve);
	public void calculerMoyenneModules(Eleve eleve);
}
