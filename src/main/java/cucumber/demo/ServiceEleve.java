package cucumber.demo;

import java.util.Iterator;

public class ServiceEleve implements IServiceEleve{
	
	public boolean getResultatExamen(Eleve eleve) {

		return eleve.getNoteMoyenne() < 60;
	}
	
	public void calculerMoyenneModules(Eleve eleve) {
		if(eleve.getNoteModules().size()>0){
			Iterator<Integer> notes=eleve.getNoteModules().values().iterator();
			int sommeNotes=0;
			while(notes.hasNext()){
				sommeNotes=sommeNotes+notes.next();
			}
			eleve.setNoteMoyenne((double) sommeNotes/eleve.getNoteModules().size());
		}
		else{eleve.setNoteMoyenne(0);}
	}
}
