package cucumber.demo.runners;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		tags = {"@Principale"},
		plugin = {"pretty", "html:target/cucumber", "json:target/cucumber.json"},
		features="src/test/resources/features/",
		glue = {"cucumber.demo.test.definitions"})
public class RunAll {

}
