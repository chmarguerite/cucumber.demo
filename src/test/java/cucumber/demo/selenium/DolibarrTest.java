package cucumber.demo.selenium;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.junit.*;

public class DolibarrTest {

	@Test
	public void testConnexion() throws MalformedURLException {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("start-maximized");
		options.setExperimentalOption("useAutomationExtension", false);
		//ChromeDriver driver = new ChromeDriver(options);
		String sUrl = "http://t3wf2:4444/wd/hub";
		 WebDriver driver = new RemoteWebDriver(new URL(sUrl), options);
		 
		 
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		driver.get("http://demo.testlogiciel.pro/dolibarr/");
		driver.findElement(By.id("username")).click();
		driver.findElement(By.id("username")).sendKeys("jsmith");
		driver.findElement(By.id("password")).click();
		driver.findElement(By.id("password")).sendKeys("hp");
		driver.findElement(By.className("button")).click();
		Assert.assertEquals("Espace accueil",driver.findElement(By.cssSelector("div.titre")).getText());
		Assert.assertTrue(driver.findElement(By.tagName("body")).getText().contains("Tableau de bord de travail"));
		System.out.println(driver.findElement(By.cssSelector("div.titre")).getText());
		driver.close();
	}
}
