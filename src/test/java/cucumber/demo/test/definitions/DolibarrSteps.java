package cucumber.demo.test.definitions;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class DolibarrSteps {
	private ChromeDriver driver;
	
	@Before("@login")
	public void initialisation() {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("start-maximized");
		options.setExperimentalOption("useAutomationExtension", false);
		driver = new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}
	
	@After("@login")
	public void fermeture() {
		driver.close();
	}
	
	@Given("^Je lance Dolibarr$")
	public void je_lance_Dolibarr() throws Throwable {
		driver.get("http://demo.testlogiciel.pro/dolibarr/");
	}

	@Given("^Je suis un utilisateur non authentifi�$")
	public void je_suis_un_utilisateur_non_authentifi�() throws Throwable {
		
		ExpectedConditions.visibilityOf(driver.findElement(By.id("username")));
	}

	@When("^Je me connecte avec les identifiants (.*):(.*)")
	public void je_me_connecte_avec_les_identifiants_bsmith(String login, String mot_de_passe) throws Throwable {
		driver.findElement(By.id("username")).click();
		driver.findElement(By.id("username")).sendKeys(login);
		driver.findElement(By.id("password")).click();
		driver.findElement(By.id("password")).sendKeys(mot_de_passe);
		driver.findElement(By.className("button")).click();
	}



	@Then("^Il est affich� \"([^\"]*)\"$")
	public void il_est_affich�(String message) throws Throwable {
	    Assert.assertEquals(message,driver.findElement(By.cssSelector("div.error")).getText());
	}

}
