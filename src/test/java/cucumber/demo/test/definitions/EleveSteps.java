package cucumber.demo.test.definitions;

import java.util.Map;

import org.junit.*;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.demo.Eleve;
import cucumber.demo.ServiceEleve;

public class EleveSteps {
	private Eleve eleve;
	private ServiceEleve serviceEleve = new ServiceEleve();
	
	@Given("^j'ai un �l�ve nomm� \"([^\"]*)\"$")
	public void j_ai_un_�l�ve_nomm�(String nomEleve) throws Throwable {
	    eleve = new Eleve();
	    eleve.setNom(nomEleve);
	    
	}

	@When("^cet �l�ve a obtenu une note moyenne de (\\d+) � l'examen$")
	public void cet_�l�ve_a_obtenu_une_note_moyenne_de_�_l_examen(int noteMoyenne) throws Throwable {
		eleve.setNoteMoyenne(noteMoyenne);
	}

	@Then("^cet �l�ve a r�ussi l'examen$")
	public void cet_�l�ve_a_r�ussi_l_examen() throws Throwable {
		Assert.assertTrue(serviceEleve.getResultatExamen(eleve));
	}

	@Then("^cet �l�ve a �chou� l'examen$")
	public void cet_�l�ve_a_�chou�_l_examen() throws Throwable {
		Assert.assertFalse(serviceEleve.getResultatExamen(eleve));
	}

	@When("^cet �l�ve a obtenu les notes suivantes$")
	public void cet_�l�ve_a_obtenu_les_notes_suivantes(DataTable arg1) throws Throwable {
	    Map<String,Integer> noteModules = (Map<String, Integer>) arg1.asMap(String.class, Integer.class);
	    eleve.setNoteModules(noteModules);
	}

	@When("^je calcule la moyenne$")
	public void je_calcule_la_moyenne() throws Throwable {
		serviceEleve.calculerMoyenneModules(eleve);
	}

	@Then("^cet �l�ve a une note moyenne de (\\d+)$")
	public void cet_�l�ve_a_une_note_moyenne_de(int arg1) throws Throwable {
		Assert.assertEquals(arg1, eleve.getNoteMoyenne(),0);
	}

}
