@Principale
Feature: Notes
En tant qu'examinateur, je dois pouvoir gérer les notations d'un élève

Background:
Given j'ai un élève nommé "Milouse Van Houten"

@NonRegression
Scenario: Résultat correct d'un élève
When cet élève a obtenu une note moyenne de 60 à l'examen
Then cet élève a réussi l'examen

Scenario: Résultat insuffisant d'un élève
When cet élève a obtenu une note moyenne de 40 à l'examen
Then cet élève a échoué l'examen

Scenario Outline: Résultat correct de plusieurs élèves
Given j'ai un élève nommé "<nom>"
When cet élève a obtenu une note moyenne de <note> à l'examen
Then cet élève a réussi l'examen

Examples:
|	nom				|	note	|
|	Homer Simpson	|	60		|
|	Bart Simpson	|	61		|
|	Maguy Simpson	|	99		|
|	Lisa Simpson	|	100		|

Scenario: Note moyenne des modules d'un élève
When cet élève a obtenu les notes suivantes
|	Java		|	85		|
|	Cucumber	|	62		|
|	Selenium	|	36		|
When je calcule la moyenne
Then cet élève a une note moyenne de 61