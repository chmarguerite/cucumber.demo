@login
Feature: Connexion
  En tant qu’utilisateur non authentifié,
  Je dois pouvoir me connecter avec des identifiants valides
  Afin de protéger les données de l'application

Background: 
	Given Je lance Dolibarr

  @1-Haute
  Scenario Outline: Test connexion avec des identifiants invalides
    Given Je suis un utilisateur non authentifié	
 
    When  Je me connecte avec les identifiants <login>:<mot de passe>
    Then Je suis un utilisateur non authentifié
     And Il est affiché "<message>"
  
  Examples:
  |	login					|	mot de passe	|	message																				|
  |	bsmith				|								|	Identifiants login ou mot de passe incorrect	|
  |	bsmith				|	mauvaismdp 		|	Identifiants login ou mot de passe incorrect	|
  |	mauvaislogin	|	mauvaismdp 		|	Identifiants login ou mot de passe incorrect	|  

